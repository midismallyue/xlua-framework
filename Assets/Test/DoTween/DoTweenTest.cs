﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using XLua;

public class DoTweenTest : MonoBehaviour {

	public TextAsset asset;
	LuaEnv luaenv;
	void Start () {
		var obj = GameObject.Find ("Cube");
		Debug.Log ("cube: " + obj);
		// var zz = transform.DOMove (Vector3.zero, 1);
		// zz.OnComplete (() => print (11));
		luaenv = new LuaEnv ();
		luaenv.DoString (asset.text);

	}

	// Update is called once per frame
	void Update () {

	}

	private void OnDestroy () {
		luaenv.Dispose ();
	}
}